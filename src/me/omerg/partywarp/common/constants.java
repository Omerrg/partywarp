package me.omerg.partywarp.common;

import org.bukkit.ChatColor;

public class constants {
    public static final String PLUGIN_NAME = "DropParties";
    public static final boolean DEBUG = true;
    public static final String DEBUG_PREFIX =
            ChatColor.BLACK + "[" + ChatColor.RED + "Drop" + ChatColor.WHITE + "Parties" + ChatColor.BLACK + "]";
    public static final String CONFIGURATION_FILE_EXTENSION = ".yml";
    public static final String PARTY_DISBANDED_MESSAGE = ChatColor.YELLOW + "Party disbanded.";
    public static final String ALREADY_IN_PARTY_MESSAGE = ChatColor.YELLOW + "Already in a party.";
    public static final String NEW_PARTY_MESSAGE = ChatColor.AQUA + "New party was created!";
    public static final String HELP_MESSAGE = ChatColor.AQUA + "Party Warp Commands:" +
            ChatColor.AQUA + "pw create,disband,leave,accept,decline,invite,createwarp,kick";
}
