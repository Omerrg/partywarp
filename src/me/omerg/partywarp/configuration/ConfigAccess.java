package me.omerg.partywarp.configuration;

import me.omerg.partywarp.PartyWarp;
import me.omerg.partywarp.common.constants;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class ConfigAccess {
    private File configurationFile;
    public FileConfiguration loadedConfiguration;
    private String configName;

    ConfigAccess(String path, boolean forceCreation) {
        this.configurationFile = new File(PartyWarp.getInstance().getDataFolder(),
                String.format("%1$s%2$s", path, constants.CONFIGURATION_FILE_EXTENSION));
        this.configName = this.configurationFile.getName();
        if (forceCreation && !this.configurationFile.exists()) {
            this.configurationFile.getParentFile().mkdirs();
            try {
                this.configurationFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        this.loadedConfiguration = YamlConfiguration.loadConfiguration(this.configurationFile);
    }

    String getName() {
        return this.configName;
    }

    public FileConfiguration getConfig() {
        return this.loadedConfiguration;
    }

    public void set(String key, Object value) {
        this.loadedConfiguration.set(key, value);
        try {
            this.loadedConfiguration.save(this.configurationFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object get(String key, Object defaultValue) {
        if (this.loadedConfiguration.contains(key)) return this.loadedConfiguration.get(key);
        else return defaultValue;
    }
    public void save() {
        try {
            this.loadedConfiguration.save(this.configurationFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Object get(String key) {
        return this.get(key, null);
    }
}
