package me.omerg.partywarp.configuration;

import java.util.ArrayList;
import java.util.List;

public class ConfigManager {
    private List<ConfigAccess> accesses;

    public ConfigManager() {
        this.accesses = new ArrayList<>();
    }

    public ConfigAccess getAccess(String configName) {
        return this.getAccess(configName, true);
    }

    private List<ConfigAccess> getAccesses() {
        return this.accesses;
    }

    public ConfigAccess getAccess(String configName, boolean forceCreation) {
        for (ConfigAccess ca : this.getAccesses())
            if (ca.getName().equalsIgnoreCase(configName))
                return ca;

        ConfigAccess configAccess = new ConfigAccess(configName, forceCreation);
        this.accesses.add(configAccess);
        return configAccess;
    }
}
