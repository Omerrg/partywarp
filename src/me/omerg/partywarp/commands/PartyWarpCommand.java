package me.omerg.partywarp.commands;

import me.omerg.partywarp.common.constants;
import me.omerg.partywarp.party.Party;
import me.omerg.partywarp.party.PartyManager;
import me.omerg.partywarp.warps.WarpManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.UUID;

public class PartyWarpCommand extends BukkitCommand {
    private static final String[] ALIASES = {"pw", "partywarp", "party"};
    private static final String DESCRIPTION = "PartyWarp main command. ";
    private static final String USAGE_MESSAGE = "/pw help";

    PartyWarpCommand() {
        super(constants.PLUGIN_NAME, DESCRIPTION, USAGE_MESSAGE, Arrays.asList(ALIASES));
    }

    @Override
    @SuppressWarnings("deprecation")
    public boolean execute(CommandSender commandSender, String label, String[] args) {
        if (!(commandSender instanceof Player))
            return false;
        Player player = (Player) commandSender;
        if (args.length == 0) {
            player.sendRawMessage(PartyManager.getPartyInfo(PartyManager.getPartyByPlayer(player.getUniqueId())));
            return true;
        } else if (args.length == 1) {
            Party party = null;
            switch (args[0]) {
                case "help":
                    return sendHelpMessage(player);
                case "create":
                    if (PartyManager.getPartyByPlayer(player.getUniqueId()) == null) {
                        new Party(player.getUniqueId());
                        player.sendRawMessage(constants.NEW_PARTY_MESSAGE);
                    } else {
                        player.sendRawMessage(constants.ALREADY_IN_PARTY_MESSAGE);
                    }
                    return true;
                case "disband":
                    party = PartyManager.getPartyByPlayer(player.getUniqueId());
                    if (party != null) {
                        if (party.getLeader() == player.getUniqueId()) {
                            party.disband();
                            player.sendRawMessage(constants.PARTY_DISBANDED_MESSAGE);
                        }
                    }
                    return true;
                case "leave":
                    party = PartyManager.getPartyByPlayer(player.getUniqueId());
                    if (party != null) {
                        party.removeMember(player.getUniqueId());
                        player.sendRawMessage(ChatColor.YELLOW + "Left party.");
                        if (Bukkit.getOfflinePlayer(party.getLeader()).isOnline()) {
                            Bukkit.getPlayer(party.getLeader()).sendRawMessage(ChatColor.RED + player.getDisplayName() + " left the party.");
                        }
                    } else {
                        player.sendRawMessage(ChatColor.YELLOW + "No party found.");
                    }

                    return true;
                case "accept":
                    PartyManager.acceptInvite(player.getUniqueId());
                    return true;
                case "decline":
                    PartyManager.declineInvite(player.getUniqueId());
                    return true;
                default:
                    OfflinePlayer op = Bukkit.getOfflinePlayer(args[0]);
                    if (op.getPlayer() != null)
                        player.sendRawMessage(PartyManager.getPartyInfo(PartyManager.getPartyByPlayer(op.getUniqueId())));
                    else
                        player.sendRawMessage(ChatColor.YELLOW + "Player not found.");
                    return true;
            }
        } else if (args.length == 2) {
            switch (args[0]) {
                case "invite":
                    PartyManager.invite(player.getUniqueId(), args[1]);
                    return true;
                case "createwarp":
                    player.sendRawMessage(WarpManager.createNewWarp(player.getLocation(), args[1]));
                    return true;
                case "kick":
                    if (PartyManager.getPartyByPlayer(player.getUniqueId()) != null) {
                        Party party = PartyManager.getPartyByPlayer(player.getUniqueId());
                        if (party.getLeader() == player.getUniqueId()) {
                            party.removeMember(Bukkit.getOfflinePlayer(args[1]).getUniqueId());
                            player.sendRawMessage(ChatColor.RED + "Player was kicked.");
                            return true;
                        }
                    }
                    player.sendRawMessage(ChatColor.RED + "Invalid task!");
                    return true;
                case "warp":
                    if (WarpManager.getWarp(args[1]) != null) {
                        if (PartyManager.getPartyByPlayer(player.getUniqueId()) != null) {
                            Party party = PartyManager.getPartyByPlayer(player.getUniqueId());
                            if (!party.getLeader().toString().equalsIgnoreCase(player.getUniqueId().toString())) {
                                System.out.println(party.getLeader().toString());
                                System.out.println(player.getUniqueId().toString());
                                player.sendRawMessage(ChatColor.RED + "You're not the leader..");
                                return true;
                            }
                            for (UUID member : party.getMembers()) {
                                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(member);
                                if (offlinePlayer.isOnline()) {
                                    offlinePlayer.getPlayer().teleport(WarpManager.getWarp(args[1]));
                                    offlinePlayer.getPlayer().sendRawMessage(ChatColor.AQUA + "Your party entered the warp: '" + args[1] + "'!");
                                }
                                return true;
                            }
                        } else {
                            player.sendRawMessage(ChatColor.RED + "You must be a part of a party!");
                        }
                    } else {
                        player.sendRawMessage(ChatColor.RED + "This warp doesn't exist..");
                    }
                default:
                    return sendHelpMessage(player);
            }
        }
        return true;
    }

    private boolean sendHelpMessage(Player p) {
        p.sendRawMessage(constants.HELP_MESSAGE);
        return true;
    }
}
