package me.omerg.partywarp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.defaults.BukkitCommand;

import java.lang.reflect.Field;


public class CommandRegistry {
    private static final BukkitCommand[] SERVER_COMMANDS = {new PartyWarpCommand()};

    public void registerCommands() {
        try {
            Field bukkitCommandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            bukkitCommandMap.setAccessible(true);
            for (BukkitCommand command : SERVER_COMMANDS)
                ((CommandMap) bukkitCommandMap.get(Bukkit.getServer())).register(command.getName(), command);
        } catch (IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException e) {
            e.printStackTrace();
        }
    }
}
