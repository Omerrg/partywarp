package me.omerg.partywarp.party;

import me.omerg.partywarp.configuration.ConfigAccess;
import me.omerg.partywarp.configuration.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PartyManager {
    private static List<Party> parties = new ArrayList<Party>();
    private static HashMap<String, String> invites = new HashMap<>();

    public static void saveParties() {
        ConfigAccess configAccess = new ConfigManager().getAccess("parties");
        for(Party party : parties) {
            List<String> stringgedUUIDs = new ArrayList<String>();
            party.getMembers().forEach(uuid -> stringgedUUIDs.add(uuid.toString()));
            configAccess.loadedConfiguration.set(party.getLeader().toString(), stringgedUUIDs);
            configAccess.save();
        }
    }

    public static void loadParties() {
        ConfigAccess configAccess = new ConfigManager().getAccess("parties");
        for(String leader : configAccess.loadedConfiguration.getKeys(false)) {
            List<UUID> members = new ArrayList<>();
            configAccess.loadedConfiguration.getStringList(leader).forEach(stringgedUUID -> members.add(UUID.fromString(stringgedUUID)));
            new Party(UUID.fromString(leader), members);
        }
    }

    @SuppressWarnings("deprecation")
    public static void invite(UUID uniqueId, String arg) {
        Player p = Bukkit.getPlayer(uniqueId);
        if(getPartyByPlayer(p.getUniqueId()) == null) {
            p.sendRawMessage(ChatColor.RED + "Can't invite to your party!");
            return;
        }
        if(getPartyByPlayer(p.getUniqueId()).getLeader() != p.getUniqueId()) {
            p.sendRawMessage(ChatColor.RED + "Can't invite to your party!");
            return;
        }
         else {
            OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(arg);
            if (invites.containsKey(offlinePlayer.getUniqueId().toString())) {
                p.sendRawMessage(ChatColor.RED + "This player has an active invite!");
                return;
            }
            if(getPartyByPlayer(offlinePlayer.getUniqueId()) != null) {
                p.sendRawMessage(ChatColor.RED + "This player is already in a party!");
                return;
            }
            if(offlinePlayer.getUniqueId() == uniqueId) {
                return;
            }
            if (offlinePlayer.isOnline()) {
                invites.put(offlinePlayer.getUniqueId().toString(), uniqueId.toString());
                p.sendRawMessage(ChatColor.AQUA + "Invite was sent");
                offlinePlayer.getPlayer().sendRawMessage(ChatColor.GREEN + "You're invited to " + p.getDisplayName() + "'s party!");
            } else {
                p.sendRawMessage(ChatColor.RED + "The player isn't online..");
            }
        }
    }

    public static void acceptInvite(UUID uniqueId) {
        Player p = Bukkit.getPlayer(uniqueId);
        if (invites.containsKey(uniqueId.toString())) {
            UUID inviterUUID = UUID.fromString(invites.get(uniqueId.toString()));
            OfflinePlayer inviter = Bukkit.getOfflinePlayer(inviterUUID);
            p.sendRawMessage(ChatColor.GREEN + "Welcome to the party!");
            if (inviter.isOnline()) {
                inviter.getPlayer().sendRawMessage(ChatColor.GREEN + p.getDisplayName() + " joined your party!");
            }
            getPartyByPlayer(inviterUUID).addMember(uniqueId);
        } else {
            p.sendRawMessage(ChatColor.RED + "No invite was found.");
        }
    }

    public static void declineInvite(UUID uniqueId) {
        Player p = Bukkit.getPlayer(uniqueId);
        if (invites.containsKey(uniqueId.toString())) {
            UUID inviterUUID = UUID.fromString(invites.get(uniqueId.toString()));
            OfflinePlayer inviter = Bukkit.getOfflinePlayer(inviterUUID);
            p.sendRawMessage(ChatColor.GREEN + "Invite declined.");
            if(inviter.isOnline()) {
                inviter.getPlayer().sendRawMessage(ChatColor.YELLOW + p.getDisplayName() + " declined your invite!");
            }
            invites.remove(uniqueId.toString());
        } else {
            p.sendRawMessage(ChatColor.RED + "No invite was found.");
        }
    }


    public static void registerParty(Party party) {
        parties.add(party);
    }

    public static Party getPartyByPlayer(UUID player) {
        for (Party party : parties) {
            for (UUID member : party.getMembers()) {
                if (member.equals(player)) {
                    return party;
                }
            }
        }
        return null;
    }

    public static String getPartyInfo(Party party) {
        if(party == null) {
            return ChatColor.RED + "No party found..";
        }
        String partyString = ChatColor.AQUA + Bukkit.getOfflinePlayer(party.getLeader()).getName() + "'s party:";
        for(UUID member : party.getMembers()) {
            partyString += ChatColor.YELLOW + "\n - " + Bukkit.getOfflinePlayer(member).getName();
        }
        return partyString;
    }

}
