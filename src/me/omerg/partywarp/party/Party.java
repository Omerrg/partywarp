package me.omerg.partywarp.party;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Party {
    private List<UUID> partyMembers;
    private UUID leader;

    public Party(UUID leader) {
        this.leader = leader;
        this.partyMembers = new ArrayList<UUID>();
        this.partyMembers.add(leader);
        PartyManager.registerParty(this);
    }

    public Party(UUID leader, List<UUID> members) {
        this.leader = leader;
        this.partyMembers = members;
        PartyManager.registerParty(this);
    }

    public void addMember(UUID member) {
        this.partyMembers.add(member);
    }

    public void removeMember(UUID member) {
        if (member.equals(leader)) {
            this.disband();
            Bukkit.getPlayer(member).sendRawMessage(ChatColor.RED + "Party was disbanded.");
        } else {
            this.partyMembers.remove(member);
        }
    }

    public UUID getLeader() {
        return this.leader;
    }

    public List<UUID> getMembers() {
        return this.partyMembers;
    }

    public void disband() {
        this.partyMembers.clear();
    }
}
