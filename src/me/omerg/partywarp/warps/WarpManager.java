package me.omerg.partywarp.warps;

import me.omerg.partywarp.configuration.ConfigAccess;
import me.omerg.partywarp.configuration.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import java.util.Arrays;
import java.util.HashMap;

public class WarpManager {
    private static HashMap<String, Location> warps = new HashMap<>();

    public static String createNewWarp(Location location, String warpName) {
        if (warps.containsKey(warpName)) {
            return ChatColor.RED + "A warp with the same name already exists.";
        } else {
            registerWarp(warpName, location);
            return ChatColor.AQUA + "Warp created!";
        }
    }

    private static void registerWarp(String warpName, Location location) {
        warps.put(warpName, location);
    }

    public static Location getWarp(String warpName) {
        return warps.get(warpName);
    }

    public static void loadWarpsFromFile() {
        ConfigAccess configAccess = new ConfigManager().getAccess("warps");
        for (String key : configAccess.loadedConfiguration.getKeys(false)) {
            warps.put(key, locationFromString(configAccess.loadedConfiguration.getString(key)));
        }
    }

    public static void saveWarpsToFile() {
        ConfigAccess configAccess = new ConfigManager().getAccess("warps");
        for (String warpName : warps.keySet()) {
            configAccess.loadedConfiguration.set(warpName, locationToString(warps.get(warpName)));
        }
        configAccess.save();
    }

    public static String locationToString(Location location) {
        return String.join("@", Arrays.asList(String.valueOf(location.getBlockX()),
                String.valueOf(location.getBlockY()), String.valueOf(location.getBlockZ()),
                location.getWorld().getName()));
    }

    public static Location locationFromString(String locationString) {
        String[] locationParts = locationString.split("@");
        return new Location(Bukkit.getWorld(locationParts[3]),
                Integer.valueOf(locationParts[0]), Integer.valueOf(locationParts[1]), Integer.valueOf(locationParts[2]));
    }
}
