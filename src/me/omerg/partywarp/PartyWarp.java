package me.omerg.partywarp;

import me.omerg.partywarp.commands.CommandRegistry;
import me.omerg.partywarp.party.PartyManager;
import me.omerg.partywarp.warps.WarpManager;
import org.bukkit.plugin.java.JavaPlugin;

public class PartyWarp extends JavaPlugin {
    private static PartyWarp instance;
    public static PartyWarp getInstance() {
        return instance;
    }

    @Override
    public void onEnable() {
        instance = this;
        new CommandRegistry().registerCommands();
        WarpManager.loadWarpsFromFile();
        PartyManager.loadParties();
    }

    @Override
    public void onDisable() {
        WarpManager.saveWarpsToFile();
        PartyManager.saveParties();
    }
}
